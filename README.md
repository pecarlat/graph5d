# Graph5D
This project contains a project I did for the [NII](http://www.nii.ac.jp/en/), to visualize efficiently 5D graphs (and, as much as possible, multi-dimensional graphs (4D, 6D, etc), using Unity and 3D mouse (not mandatory).

## Requirements
You will need Unity 5.6, and 5.5 should also be fully compatible.

## Demo
The following gif shows a quick visual example of a basic sinus function depending on 5 variables. It was shot on regular 2D screen with no 3D mouse ; dimensions were switched by hands.

![Demo](resources/demo.gif)

## Notes
The Library must be removed from the gitignore, sill need to find out why
